/**
 * BaseBean
 * A general implementation of methods for the base bean
 * Note: This is normal class without abstraction but having abstraction could streamline inheritance.
 */
package com.jms.jejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;

@Singleton
public class BaseBean implements BaseRemote {
	private static final long serialVersionUID = 1L;
	List<String> apps;
	
	//Singleton constructor
    public BaseBean() {
        apps = new ArrayList<String>();
    }

	@Override
	public void addApp(String appName) {
		apps.add(appName);
	}

	@Override
	public List<String> getApps() {
		return apps;
	}
}