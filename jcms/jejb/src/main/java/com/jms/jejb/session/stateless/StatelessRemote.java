package com.jms.jejb.session.stateless;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Remote;

@Remote
public interface StatelessRemote extends Serializable {
	void addStateless(String name);
	List<String> getStateless();
}