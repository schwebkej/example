package com.jms.jejb.session.stateful;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Remote;

@Remote
public interface StatefulRemote extends Serializable {
	void addStateful(String name);
	List<String> getStateful();
}