package com.jms.jejb.mdb;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven(mappedName="jms/ExampleQueue", activationConfig = {  
	@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class ExampleMDB implements MessageListener {
	
	@Resource
	private MessageDrivenContext mdc;
	static final Logger logger = Logger.getLogger("ExampleMDB");
	
	public ExampleMDB() {}
	
    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                logger.log(Level.INFO, "MESSAGE BEAN: Message received: {0}", ((TextMessage) message).getText());
            } else {
                logger.log(Level.WARNING, "Message of wrong type: {0}", message.getClass().getName());
            }
        } catch (JMSException e) {
            logger.log(Level.SEVERE, "ExampleMDB.onMessage: JMSException: {0}", e);
            mdc.setRollbackOnly();
        }
    }
}