package com.jms.jejb.session.stateless;

import java.util.List;

import javax.ejb.Stateless;

@Stateless
public class StatelessBean implements StatelessRemote {
	private static final long serialVersionUID = 1L;
	private List<String> stateless;
	
	@Override
	public void addStateless(String name) {
		stateless.add(name);
	}

	@Override
	public List<String> getStateless() {
		return stateless;
	}
}