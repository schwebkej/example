/**
 * BaseRemote
 * A general implementation of methods for the base bean
 * Note: This is normal class without abstraction but having abstraction could streamline inheritance.
 */
package com.jms.jejb;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface BaseRemote extends Serializable {

	//Allow bean to add string to list
	void addApp(String appName);
	
	//Allow bean to provide list of string
	List<String> getApps();
}