package com.jms.jejb.session.singleton;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Remote;

@Remote
public interface SingletonRemote extends Serializable {
	void addSingleton(String name);
	List<String> getSingleton();
}