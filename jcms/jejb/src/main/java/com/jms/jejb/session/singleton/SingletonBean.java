/**
 * SingletonBean
 * A general implementation of methods for the Singleton bean
 * Note: This is normal class without abstraction but having abstraction could streamline inheritance.
 */
package com.jms.jejb.session.singleton;

import java.util.List;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Startup
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class SingletonBean implements SingletonRemote {
	private static final long serialVersionUID = 1L;
	private List<String> singleton;

	@Lock(LockType.READ)
	public List<String> getSingleton() {
		return singleton;
	}
	
	@Lock(LockType.WRITE)
	public void addSingleton(String name) {
		singleton.add(name);
	}
}