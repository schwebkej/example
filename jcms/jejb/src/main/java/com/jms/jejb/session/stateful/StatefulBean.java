package com.jms.jejb.session.stateful;

import java.util.List;

import javax.ejb.Stateful;

@Stateful
public class StatefulBean implements StatefulRemote {
	private static final long serialVersionUID = 1L;
	private List<String> stateful;
	
	@Override
	public void addStateful(String name) {
		stateful.add(name);
	}

	@Override
	public List<String> getStateful() {
		return stateful;
	}
}