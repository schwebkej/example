<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>BaseRemote Example</title>
	</head>
	<body>
		<h2>StatelessRemote Example</h2>
		<table border="1">
			<thead>
				<tr>
					<th colspan="2">StatelessRemote</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><pre>&#36;&#123;remote&#125;</pre></td>
					<td><pre>${remote}</pre></td>
				</tr>
				<tr>
					<td><pre>&#60;&#37;&#61; out.println(getServletContext().getAttribute("remote")) &#37;&#62;</pre></td>
					<td><pre><% out.println(getServletContext().getAttribute("remote")); %></pre></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>