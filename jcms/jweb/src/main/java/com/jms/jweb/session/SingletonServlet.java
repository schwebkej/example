package com.jms.jweb.session;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.jms.jejb.session.singleton.SingletonRemote;

@WebServlet("/singleton")
public class SingletonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private SingletonRemote remote;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/session/singleton.jsp");
		getServletContext().setAttribute("remote", remote);
		dispatcher.forward(req, resp);
	}
	
	@EJB
	public void setRemote(SingletonRemote remote) {
		this.remote = remote;
	}
}