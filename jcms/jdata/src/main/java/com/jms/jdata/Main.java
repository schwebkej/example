package com.jms.jdata;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.derby.jdbc.EmbeddedDriver;

import com.jms.jdata.dto.EntityDTO;

public class Main {
	
	public static void main(String[] args) {
		testDriverManager();
		testEntityManager();
	}
	
	private static void testDriverManager() {
		try {
			
			DriverManager.registerDriver(new EmbeddedDriver());
			Connection con = DriverManager.getConnection("jdbc:derby:memory:exampledb;create=true");
		
			DatabaseMetaData dbmd = con.getMetaData();
			ResultSet md = dbmd.getTables(null, "APP", "ENTITYDTO", new String[]{"TABLE"});
			
			if(md.next()) {
				Statement del = con.createStatement();
				del.executeUpdate("DROP TABLE APP.EntityDTO");
				del.close();	
			}
			
			Statement create = con.createStatement();
			create.executeUpdate("CREATE TABLE APP.EntityDTO (id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), name VARCHAR(255) NOT NULL, jdouble FLOAT, jint INTEGER, CONSTRAINT primary_key PRIMARY KEY (id))");
			create.close();
			
			PreparedStatement ps = con.prepareStatement("INSERT INTO APP.EntityDTO (name, jint, jdouble) VALUES (?, ?, ?)");
			ps.setString(1, "An example of embedded Derby using JAVA JPA API.");
			ps.setInt(2, 50);
			ps.setDouble(3, 3.1);
			ps.executeUpdate();

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM APP.EntityDTO");
			
			while(rs.next()) {
				System.out.println("DTO id: " + rs.getString("ID") + " Name: " + rs.getString("name") + " Double: " + rs.getString("JDOUBLE") + " Integer: " + rs.getString("JINT"));
			}
			
			rs.close();
			stmt.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void testEntityManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("jdata");
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		EntityDTO newDTO = new EntityDTO();
		newDTO.setJdouble(1.1);
		newDTO.setJint(1);
		newDTO.setName("An example of embedded Derby using JAVA EntityManager API.");
		em.persist(newDTO);
		em.getTransaction().commit();
		Query q = em.createQuery("FROM EntityDTO dto");
		List<?> entities = (List<?>)q.getResultList();
		for(Object _dto : entities) {
			EntityDTO dto = (EntityDTO) _dto;
			System.out.println("DTO id: " + dto.getId() + " Name: " + dto.getName() + " Double: " + dto.getJdouble() + " Integer: " + dto.getJint());
		}
		em.close();
	}
}
