package com.jms;

import static org.junit.Assert.*;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.jms.jejb.BaseRemote;
import com.jms.jejb.session.singleton.SingletonRemote;
import com.jms.jejb.session.stateful.StatefulRemote;
import com.jms.jejb.session.stateless.StatelessRemote;

public class JEJBTest {
	
	private static EJBContainer container;
	private BaseRemote baseRemote = null;
	private StatelessRemote statelessRemote = null;
	private StatefulRemote statefulRemote = null;
	private SingletonRemote singletonRemote = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//Embeddable EJB Container - because this test should not connect to a running GlassFish EJB server.
		container = EJBContainer.createEJBContainer();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		container.close();
	}

	@Before
	public void setUp() throws Exception {
		assertNotNull(container);
	}

	@After
	public void tearDown() throws Exception {
		baseRemote = null;
	}

	@Test
	public void testBaseRemote() {
		try {
			//Search for Portable JNDI generated from emeddable GlassFish EJB Container 
			Object object = container.getContext().lookup("java:global/classes/BaseBean");
			//Base test to confirm a singleton bean reference
	        assertTrue(object instanceof BaseRemote);
	        baseRemote = (BaseRemote) object;
		} catch(NamingException e) {
			//Something went wrong - fail the test
			e.printStackTrace();
			fail();
		}
		assertNotNull(baseRemote);
		System.out.println("["+baseRemote+"] test completed.");
	}
	
	@Test
	public void testSingletonRemote() {
		try { 
			Object object = container.getContext().lookup("java:global/classes/SingletonBean");
	        assertTrue(object instanceof SingletonRemote);
	        singletonRemote = (SingletonRemote) object;
		} catch(NamingException e) {
			e.printStackTrace();
			fail();
		}
		assertNotNull(singletonRemote);
		System.out.println("["+singletonRemote+"] test completed.");
	}
	
	@Test
	public void testStatefulRemote() {
		try { 
			Object object = container.getContext().lookup("java:global/classes/StatefulBean");
	        assertTrue(object instanceof StatefulRemote);
	        statefulRemote = (StatefulRemote) object;
		} catch(NamingException e) {
			e.printStackTrace();
			fail();
		}
		assertNotNull(statefulRemote);
		System.out.println("["+statefulRemote+"] test completed.");
	}
	
	@Test
	public void testStatelessRemote() {
		try { 
			Object object = container.getContext().lookup("java:global/classes/StatelessBean");
	        assertTrue(object instanceof StatelessRemote);
	        statelessRemote = (StatelessRemote) object;
		} catch(NamingException e) {
			e.printStackTrace();
			fail();
		}
		assertNotNull(statelessRemote);
		System.out.println("["+statelessRemote+"] test completed.");
	}
}