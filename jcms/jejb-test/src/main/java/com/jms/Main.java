package com.jms;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.jms.jejb.BaseRemote;

public class Main {

	public static void main(String[] args) {
		try {
			//We are connecting to a running GlassFish EJB server
			Context ctx = new InitialContext();
			BaseRemote bean = (BaseRemote)ctx.lookup("java:global/jear/jejb-1.0.0-SNAPSHOT/BaseBean");
			//Embeddable
			//BaseRemote bean = (BaseRemote)ctx.lookup("java:global/jear/jejb-1.0.0-SNAPSHOT/BaseBean");

			bean.addApp("A string to add in list.");
			System.out.println(bean.getApps());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}