package com.jms.mdb;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public class ExampleMDBClient {

	static final Logger logger = Logger.getLogger("ExampleMDBClient");
	
	public static void main(String[] args) {
		String text;
        final int NUM_MSGS = 3;

        try {
        	Context ctx = new InitialContext();
        	ConnectionFactory connectionFactory = (ConnectionFactory) ctx.lookup("jms/ConnectionFactory");
        	Destination queue = (Destination) ctx.lookup("jms/ExampleQueue");
        	Connection connection = connectionFactory.createConnection();
        	connection.start();
        	Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        	MessageProducer mp = session.createProducer(queue);

            for (int i = 0; i < NUM_MSGS; i++) {
                text = "This is message " + (i + 1);
                System.out.println("Sending message: " + text);
                TextMessage message = session.createTextMessage(text);
                mp.send(message);
            }

            System.out.println("To see if the bean received the messages, check <glassfish_install_dir>/domains/domain1/logs/server.log.");
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception occurred: {0}", e);
        } 
        System.exit(0);
	}
}