Example Development

Prerequisites
Java 1.7.0_79 ProgrammingFramework
http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html 
Git 2.5.0 Source Management
https://git-scm.com/downloads 
Maven 3.3.3 Project Management
https://maven.apache.org/download.cgi 
Eclipse IDE for Java EE Developers
https://eclipse.org/downloads/ 
GlassFish Application Server
http://www.oracle.com/technetwork/middleware/glassfish/downloads/ogs-3-1-1-downloads-439803.html 

Git Source Repository
https://schwebkej@bitbucket.org/schwebkej/example.git

Step(s):

Install Java, Git, Maven, and Eclipse IDE

1. Download and install Java should be done first because of dependency for Eclipse and Maven.
2. As soon as that is done, install Git. 
3. After that, create a directory, C:\Apps, to download and unzip the GlassFish and Maven files. 
4. Eclipse IDE should be in Program Files directory, or C:\Apps, depending on your preference. 
5. Add the directory path to Java bin, GlassFish bin, and Maven bin to PATH variable. 
6. Create the following variables and set accordingly. 
1. JAVA_HOME = C:\Program Files\Java\jdk1.7.0_79 
2. MAVEN_HOME = C:\Apps\apache-maven-3.3.3 
3. M2_HOME = C:\Apps\apache-maven-3.3.3
7. Verify your installation is successful by starting up Eclipse IDE and running the following commands for prompt to display output.
1. javac --version
2. java --version
3. mvn --version
4. In Git Bash: git --version

In this example, there are two ways to check out a project from repository. One method is cloning using Git Bash, and second method is having Eclipse to check out project. 

Method #1
Git Bash
1. Start > Program Files > Git Bash.
2. Create a Site folder in home directory: mkdir ~/Sites
3. Navigate to Sites directory: cd ~/Sites
4. Clone the Example repository: git clone https://schwebkej@bitbucket.org/schwebkej/example.git
5. Navigate into jcms directory from downloaded Example folder.
6. Download dependencies and build project: mvn clean install

Method #2
Eclipse Git Perspective
1. Start up Eclipse IDE.
2. Open the Git Perspective: Window > Perspective > Open Perspective > Other and select Git.
3. In Git Repository tabbed window, click on “Clone a Git repository”.
1. URI: https://schwebkej@bitbucket.org/schwebkej/example.git
2. Enter your authentication credentials.
3. Click Next.
4. Ensure master branch has a checkbox checked.
5. Click Next.
6. Click Finish.

Eclipse Import Projects
1. Start Eclipse and open Git Perspective.
1. If you had cloned an existing repository then click “Add existing local repository”.
1. By default, Eclipse looks for git folder in home directory.
2. Right-click example [master] repository in the Git Repository tabbed window.
1. Select Import Projects.
2. Click Next.
3. Ensure all projects has checkbox checked.
4. Click Finish.
3. Run command to build and install Example module: mvn clean install
4. Open Java EE perspective.
1. Right-click jcms folder: Maven > Update Project.
2. Errors should disappear from the workspace except two JPA warnings.
5. Eclipse need to know about Apache Derby as embedded database
1. Right-click jdata and select Properties.
2. Select JPA then click “Add connection...” in Connection section.
3. Select Derby and enter “EmbedDerby” and “An embedded Derby database” in the name and description then click Next.
4. Click on the icon right next to Drivers drop down box to define a new Driver.
5. Select Derby Embedded JDBC Driver 10.1 Version.
6. Navigate to JAR List tab.
7. Select derby.jar then click on “Edit JAR/Zip...” button.
1. At this point, Maven should have already downloaded dependencies including Apache Derby.
2. Find ~/.m2/repository/org/apache/derby/derby/10.10.1.1/derby-10.10.1.1.jar file.
8. Click on Properties tab.
1. Connection URL: jdbc:derby:memory:exampledb;create=true
2. Database Name: exampledb
9. Click OK.
10. Click Test Connection button to ensure Apache Derby is configured correctly.
11. Ensure Create database checkbox is checked.
12. Click Next then Finish and Apply.
13. Right-click jdata-test and select properties.
14. Click JPA and choose “EmbedDerby” from Connection drop down menu.
15. Click Apply then OK.
Apache Derby Connection
1. Choose Window > Show View > Other and type Data Source Explorer in the text box.
2. Click OK to show the view in one of the active windows.
3. Expand Database Connections folder in the Data Source Explorer.
1. EmbedDerby should be visible in the view as a data source.
2. Right-click EmbedDerby and select Connect.
3. A new database should be visible beneath EmbedDerby data source.
4. Right-click jdata/jdata-test and select Validate.
1. The JPA warning for both module goes away.
5. To fix the “Table EntityDTO cannot be resolved” error.
1. Right-click jdata and select Generate Tables from Entities from JPA menu.
2. Select Sql-script mode and click Finish.
1. The process looks at Annotated classes and automatically create a createDDL.sql file for loading into Derby.
3. Click on createDDL.sql tab to open the file.
1. Select Derby_10.x from Type drop down menu.
2. Select EmbedDerby from Name drop down menu.
3. Select memory:exampledb from Database drop down menu.
4. Place the cursor anywhere on the SQL statement then press the key combos.
1. ALT + S to execute the current statement. 
2. CTRL + ALT + X to execute all SQL statement.
5. SQL Results tabbed window should open up automatically to display result.
6. Close and discard createDDL.sql as this can be auto-generated.
7. Right-click EmbedDerby in Data Source Explorer view and select Refresh.
8. Right-click jdata in Project Explorer view and select Validate.

GlassFish Deployment
1. Run GlassFish commands
1. asadmin create-domain ear-ejb-war
2. asadmin start-domain ear-ejb-war
3. asadmin create-jms-resource --restype javax.jms.ConnectionFactory jms/ExampleConnectionFactory
4. asadmin create-jms-resource --restype javax.jms.Queue --property Name=ExampleQueue jms/ExampleQueue
2. Navigate to jcms folder in ~/git/example directory.
3. Build the project: mvn clean install 
1. Note the EAR file location from Maven output.
4. Run GlassFish command:
1. asadmin deploy jear/target/jear.ear
2. There shouldn't be any error at this point and console output “Application deployed with name jear.”

Running Examples:
Web
1. Open web browser and navigate to http://localhost:8080/
1. Base: /base
2. Stateless: /stateless
3. Stateful: /stateful
4. Singleton: /singleton

Java
1. Open ExampleMDBClient.java from jejb-test project in Eclipse.
1.  Right-click ExampleMDBClient and choose Run As > Java Application.
2. The Java console should output the result, and the GlassFish server.log logs the received message.